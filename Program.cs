﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassiividjaTsüklid
{
    class Program
    {
        static void Main(string[] args)
        {

            // eile vaatasime muutujat ja andmetüüpi

            /* int arv = 78; */  // siin lauses on KOLM komponenti:
                                 // 1. muutuja nimi arv                          kirjeldatav osa
                                 // 2. tema andmetüüp + int (või System,Int32)   kirjeldatav osa
                                 // 3. väärtustamisavaldis: = 78                 täidetav osa

            // muutuja peab ENNE kasutamist (mõnes avaldises või üksi) olema enne defineeritud ja saanud väärtuse
            // välja arvatud omistamisavaldise vasakul pool - siis ta peab enne olema lihtsalt defineeritud

            //int[] arvud;    // tegemist on int-ide massiiviga, kes on neid PALJU (huvitav, kui palju)

            //arvud = new int[10];
            //arvud[7] = 78;
            //Console.WriteLine(arvud[4]);

            //int[] teine = { 1, 2, 37, 4, 5 };            //algväärtustamise avaldis

            //// C# keeles on veel kaks massiivi liiki

            //int[][] ohoo = new int[10][];               // massiivide massiiv
            //ohoo[7] = new int[] { 1, 2, 3, 4, 5, 6 };

            //int[,] ahaa = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; // kahemõõtmeline massiiv

            //Console.WriteLine(ohoo[7][3]); // na4inaet s4itat s 0, zna4it 3ke sootvetstvuet 4islo 4, wto pishet v konsol
            //Console.WriteLine(ahaa[2, 1]); // sna4ala s4itaet massivy s 0, 2ke sootvetstvue t 3i massiv i v njom s4itaja s nulja 4ilsu 1 sootvetstvuet 8, eto pishet v konsol

            //// nüüd saame õppida selgeks uue viguri - tsüklid

            //for(int i = 0; i < arvud.Length; i++)   // tsüklides saab kasutada ühetähelisi muutujaid, nagu i,a etc
            //{
            //    arvud[i] = i * i;                   // see tsükkel kirjutab massiivi järjekorra numbreid ruudus?
            //}

            //for (int i = 0; i < arvud.Length;)
            //{
            //    Console.WriteLine(arvud[i++]);
            //}

            //foreach (int x in arvud) Console.WriteLine(x);

            // siis on midagi teismoodi, öwrfs,m vnalerio











            // int ii = 0;

            //while ( ii < arvud.Length )
            //{
            //    Console.WriteLine(arvud[ii++]);
            //}

            //for(; ii < arvud.Length;)               // while tsükli on for tsükli alternatiiv
            //{
            //    Console.WriteLine(arvud[ii]);
            //    ii++;
            //}

            //Console.WriteLine("Mis värvi tuli seal on: ");
            //string tuli = Console.ReadLine();
            //if (tuli == "punane") Console.WriteLine("jää seisma!");
            //else if (tuli == "kollane") Console.WriteLine("oota rohelist");
            //else if (tuli == "roheline") Console.WriteLine("sõida edasi");

            //bool kasRoheline = false;
            //while (!kasRoheline)
            //{
            //    Console.Write("no mis värvi tuli seal on: ");
            //    switch (Console.ReadLine().ToLower())
            //    {
            //        case "punane":
            //            Console.WriteLine("oota rohelist");
            //            break;
            //        case "kollane":
            //            Console.WriteLine("oota rohelist");
            //            break;
            //        case "roheline":
            //            Console.WriteLine("sõida edasi");
            //            kasRoheline = true;
            //            break;

            //    }
            //}

            //Random r = new Random();
            //// selline väike asjake on juhuslike arvude tekitaja
            //int juhus = r.Next(); // annab juhuslike arvu
            //int juhus = r.Next(100); //annab juhusliku arvu vahenikus 0...99


            //Random r = new Random();
            //int size = r.Next(5, 20);
            //int[] mass = new int[size];
            //for (int i = 0; i < size; i++)
            //{
            //    mass[i] = r.Next(100);
            //    Console.WriteLine($)
            // }


            // Peastarvutamise test
            
            Random r = new Random();
            int punkte = 50;
            for(int test = 0; test < 10; test++)
            {
                int üks = r.Next(100);
                int teine = r.Next(100);
                int vastus = üks + teine;
                for (int katse = 0; katse < 5; katse++) ;
                {
                    Console.Write($"leia arvude {üks} ja {teine} summa: ");
                    string tulemus = Console.Readline();
                    if (int.TryParse(tulemus, out int tulem)) ;
                    {
                        if (vastus == tulem) break;
                        punkte--;

                    }
                    else
                    {
                        Console.WriteLine("vasta korralikult");
                    }
                    punkte--;
                    Console.Write
                }
            }
           
            Console.Writeline($"said kokku {punkte} punkti"");
                // Annab hinde kuju - suurepärane (45-50),
                // väga hea (35 - 44), hea (25-34), rahuldav (15 -24), puudulik (5-14)
                // nõrk (0-4)

            string[] hinded = { "nõrk", "puudulik", "rahuldav", "hea", "väga hea", "suurepärane"};
            int hinne = (int)Math,Round(punkte / 10.0);
            Console.WriteLine($"sinu hinne on {hinded[hinne]}");

        }



    }
}
